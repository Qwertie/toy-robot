class ToyRobot::Robot
  BOARD_SIZE = [4,4].freeze
  FACING_DIRECTIONS = ['NORTH', 'EAST', 'SOUTH', 'WEST'].freeze

  def initialize
    @x = nil
    @y = nil
    @facing = nil
  end

  def place(x,y,facing)
    return nil unless place_valid?(x,y,facing)

    @x = x
    @y = y
    @facing = facing
  end

  def turn_right
    return nil unless is_placed?
    @facing = FACING_DIRECTIONS[(FACING_DIRECTIONS.index(@facing) + 1) % FACING_DIRECTIONS.size]
  end

  def turn_left
    return nil unless is_placed?
    @facing = FACING_DIRECTIONS[FACING_DIRECTIONS.index(@facing) - 1 % FACING_DIRECTIONS.size]
  end

  def move
    return nil unless can_move?
    @x, @y = move_destination
  end

  def report
    return nil unless is_placed?
    "#{@x},#{@y},#{@facing}"
  end

  private

  def is_placed?
    !(@x.nil? || @y.nil? || @facing.nil?)
  end

  def place_valid?(x,y,facing)
    return false unless location_valid?(x,y)
    return false unless FACING_DIRECTIONS.include?(facing)
    true
  end

  # Return the x,y of where the robot will be after a move
  def move_destination
    if @facing == 'NORTH'
      [@x, @y + 1]
    elsif @facing == 'EAST'
      [@x + 1, @y]
    elsif @facing == 'SOUTH'
      [@x, @y - 1]
    elsif @facing == 'WEST'
      [@x - 1, @y]
    else
      raise 'facing invalid'
    end
  end

  # Valid if within the configured board size
  def location_valid?(x,y)
    return false if x < 0
    return false if x > BOARD_SIZE[0]
    return false if y < 0
    return false if y > BOARD_SIZE[1]
    true
  end

  def can_move?
    return false unless is_placed?
    return location_valid?(*move_destination)
  end
end
