# frozen_string_literal: true

RSpec.describe ToyRobot::Robot do
  describe '#place' do
    it "can place robot correctly" do
      robot = described_class.new
      robot.place(0, 1, 'NORTH')
      expect(robot.report).to eq('0,1,NORTH')

      robot.place(4, 1, 'SOUTH')
      expect(robot.report).to eq('4,1,SOUTH')
    end
  end

  describe '#report' do
    it 'returns nil before being placed' do
      robot = described_class.new
      expect(robot.report).to be(nil)
    end

    it 'prints report after being placed' do
      robot = described_class.new
      robot.place(4, 1, 'EAST')
      expect(robot.report).to eq('4,1,EAST')
    end
  end

  describe '#turn_right' do
    it 'turns right' do
      robot = described_class.new
      robot.place(0, 1, 'NORTH')

      robot.turn_right
      expect(robot.report).to eq('0,1,EAST')
      robot.turn_right
      expect(robot.report).to eq('0,1,SOUTH')
      robot.turn_right
      expect(robot.report).to eq('0,1,WEST')
      robot.turn_right
      expect(robot.report).to eq('0,1,NORTH')
    end
  end

  describe '#turn_left' do
    it 'turns left' do
      robot = described_class.new
      robot.place(0, 1, 'NORTH')

      robot.turn_left
      expect(robot.report).to eq('0,1,WEST')
      robot.turn_left
      expect(robot.report).to eq('0,1,SOUTH')
      robot.turn_left
      expect(robot.report).to eq('0,1,EAST')
      robot.turn_left
      expect(robot.report).to eq('0,1,NORTH')
    end
  end

  describe '#move' do
    it 'accepts valid moves' do
      robot = described_class.new
      robot.place(0, 1, 'NORTH')

      robot.move
      expect(robot.report).to eq('0,2,NORTH')
      robot.move
      expect(robot.report).to eq('0,3,NORTH')
      robot.move
      expect(robot.report).to eq('0,4,NORTH')

      robot.place(2,4,'SOUTH')

      robot.move
      expect(robot.report).to eq('2,3,SOUTH')
      robot.move
      expect(robot.report).to eq('2,2,SOUTH')
      robot.move
      expect(robot.report).to eq('2,1,SOUTH')
      robot.move
      expect(robot.report).to eq('2,0,SOUTH')
    end

    it 'ignores out of bounds moves' do
      robot = described_class.new
      robot.place(4,4,'NORTH')
      robot.move
      expect(robot.report).to eq('4,4,NORTH')
    end
  end
end
