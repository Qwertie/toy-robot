# frozen_string_literal: true

require_relative "toy_robot/version"
require_relative "toy_robot/robot"
require_relative "toy_robot/runner"

module ToyRobot
  class Error < StandardError; end
end
