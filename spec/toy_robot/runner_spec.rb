RSpec.describe ToyRobot::Runner do
  describe '#run' do
    it 'runs and prints the result' do
      expect do
        described_class.new.run('spec/example_input.txt')
      end.to output("3,3,NORTH\n").to_stdout
    end
  end
end